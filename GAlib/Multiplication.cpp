#include "pch.h"
#include "Multiplication.h"

int mult_karatsuba(int X, int Y)
{
	int size_X = (int)log10((double)X) + 1;
	int size_Y = (int)log10((double)Y) + 1;
	int n = 1;
	if (std::max(size_X, size_Y) != 1)
	{
		n = std::max(ceil((double)size_X / 2) * 2, ceil((double)size_Y / 2) * 2);
	}

	if (n == 1)
	{
		return X * Y;
	}
	int left_part_first_val = X / (int)pow(10, n / 2);
	int right_part_first_val = X % (int)pow(10, n / 2);
	int left_part_second_val = Y / (int)pow(10, n / 2);
	int right_part_second_val = Y % (int)pow(10, n / 2);
	int AC = mult_karatsuba(left_part_first_val, left_part_second_val);
	int BD = mult_karatsuba(right_part_first_val, right_part_second_val);
	int ABCD = mult_karatsuba(left_part_first_val + right_part_first_val, left_part_second_val + right_part_second_val);
	return AC * pow(10, n) + (ABCD - AC - BD) * pow(10, n / 2) + BD;
}